;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-development-elisp
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("org-development-elisp-core"
                       "org-development-elisp-mode"
                       "org-development-elisp")
  test-phase-enabled nil
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-prepare :before
            (lambda (&rest _) (require 'org-src-elisp-extras)))
