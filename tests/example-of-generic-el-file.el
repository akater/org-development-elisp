;; -*- lexical-binding: t; -*-

(eval-when-compile (require 'cl-macs))
(require 'cl-seq)

;; standalone comment

(defun lorem (ipsum))

;;;###autoload (autoload 'doloret "sit" nil t)
(defun doloret (sit))

(defun f0 (x))
(defalias 'f 'f0)


;; two lines of
;; standalone comment

;; non-autoload comment sticked before definition
(defun lorem (ipsum))

;; non-autoload comment sticked before definition
;;;###autoload (autoload 'doloret "sit" nil t)
(defun doloret (sit))

;; non-autoload comment sticked before definition
(defun f0 (x))
(defalias 'f 'f0)


;; three lines
;; of
;; standalone comment

(defun lorem (ipsum))
;; non-autoload comment sticked after definition

;;;###autoload (autoload 'doloret "sit" nil t)
(defun doloret (sit))
;; non-autoload comment sticked after definition

(defun f0 (x))
(defalias 'f 'f0)
;; non-autoload comment sticked after definition


;; four
;; lines
;; of
;; standalone comment

;; non-autoload comment sticked before definition
(defun lorem (ipsum))
;; non-autoload comment sticked after definition

;; non-autoload comment sticked before definition
;;;###autoload (autoload 'doloret "sit" nil t)
(defun doloret (sit))
;; non-autoload comment sticked after definition

;; non-autoload comment sticked before definition
(defun f0 (x))
(defalias 'f 'f0)
;; non-autoload comment sticked after definition

(provide 'example-of-generic-el-file)

;;; example.el ends here
